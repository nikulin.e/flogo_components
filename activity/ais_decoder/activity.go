package ais_decoder

import (
	"encoding/json"
	"github.com/BertoldVdb/go-ais"
	"github.com/BertoldVdb/go-ais/aisnmea"
	"github.com/project-flogo/core/activity"
	"github.com/project-flogo/core/data/coerce"
)

const (
	ivData        = "data"
	ivSkipInvalid = "skip_invalid"
	ovData        = "data"
	ovDecoded     = "decoded"
	ovMsgType     = "msg_type"
	ovMMSI        = "mmsi"
)

type Input struct {
	Data        string `md:"data"`
	SkipInvalid bool   `md:"skip_invalid"`
}

func (i *Input) ToMap() map[string]interface{} {
	return map[string]interface{}{
		ivData:        i.Data,
		ivSkipInvalid: i.SkipInvalid,
	}
}

func (i *Input) FromMap(values map[string]interface{}) error {
	var err error
	i.Data, err = coerce.ToString(values[ivData])
	if err != nil {
		return err
	}
	i.SkipInvalid, err = coerce.ToBool(values[ivSkipInvalid])
	if err != nil {
		return err
	}
	return nil
}

type Output struct {
	Data    string `md:"data"`
	Decoded string `md:"decoded"`
	MsgType uint8  `md:"msg_type"`
	MMSI    uint32 `md:"mmsi"`
}

func (o *Output) ToMap() map[string]interface{} {
	return map[string]interface{}{
		ovData:    o.Data,
		ovDecoded: o.Decoded,
		ovMsgType: o.MsgType,
		ovMMSI:    o.MMSI,
	}
}

func (o *Output) FromMap(values map[string]interface{}) error {
	var err error
	o.Data, err = coerce.ToString(values[ovData])
	if err != nil {
		return err
	}
	o.Decoded, err = coerce.ToString(values[ovDecoded])
	if err != nil {
		return err
	}
	tmpInt, err := coerce.ToInt(values[ovMsgType])
	if err != nil {
		return err
	}
	o.MsgType = uint8(tmpInt)

	tmpInt64, err := coerce.ToInt64(values[ovMMSI])
	if err != nil {
		return err
	}
	o.MMSI = uint32(tmpInt64)

	return nil
}

func init() {
	_ = activity.Register(&Activity{}, New)
}

var activityMd = activity.ToMetadata(&Input{}, &Output{})

// Activity is a Counter Activity implementation
type Activity struct {
	nm *aisnmea.NMEACodec
}

func New(ctx activity.InitContext) (activity.Activity, error) {
	act := &Activity{}
	act.nm = aisnmea.NMEACodecNew(ais.CodecNew(false, false))
	return act, nil
}

// Metadata implements activity.Activity.Metadata
func (a *Activity) Metadata() *activity.Metadata {
	return activityMd
}

// Eval implements activity.Activity.Eval
func (a *Activity) Eval(ctx activity.Context) (done bool, err error) {
	// Get input data
	input := &Input{}
	err = ctx.GetInputObject(input)
	if err != nil {
		return false, err
	}

	outObj := &Output{
		Data:    input.Data,
		Decoded: "",
	}

	// Decode
	decoded, err := a.nm.ParseSentence(input.Data)
	if err != nil || decoded == nil {
		// Not Decoded
		_ = ctx.SetOutputObject(outObj)
		if input.SkipInvalid {
			return true, nil
		} else {
			return false, err
		}
	}

	jsonStr, err := json.Marshal(decoded.Packet)
	if err != nil {
		// Not Converted
		_ = ctx.SetOutputObject(outObj)
		if input.SkipInvalid {
			return true, nil
		} else {
			return false, err
		}
	}

	// Set output
	outObj.Decoded = string(jsonStr)
	outObj.MsgType = decoded.Packet.GetHeader().MessageID
	outObj.MMSI = decoded.Packet.GetHeader().UserID
	err = ctx.SetOutputObject(outObj)
	if err != nil {
		return false, err
	}

	return true, nil
}
