module gitlab.com/nikulin.e/flogo_components/activity/ais_decoder

require (
	github.com/BertoldVdb/go-ais v0.0.0-20201030122405-7f7997dedf99
	github.com/adrianmo/go-nmea v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/project-flogo/core v0.9.4-hf.1
	github.com/stretchr/testify v1.5.1
)

go 1.12
