<!--
title: TCP Client
weight: 4701
-->
# TCP Client Trigger

This trigger reads/writes data using TCP socket.

### Flogo CLI
```bash
flogo install https://gitlab.com/nikulin.e/flogo_components/trigger/tcp_client
```

## Configuration

### Setting :

| Name       | Type    | Description
|:---        | :---    | :---     
| host       | string  | Host IP or DNS resolvable name 
| port       | string  | Port to connect on - ***REQUIRED***
| delimiter  | string  | Delimiter for read and write. If not set, trigger will read data until EOF
| timeout    | integer | Read and Write timeout in milliseconds. To disable timeout, set value to 0.


### Output:

| Name         | Type     | Description
|:---          | :---     | :---   
| data         | string   | The data received from client

### Reply:

| Name         | Type     | Description
|:---          | :---     | :---   
| reply        | string   | The data to be sent back to the client

## Examples

```json
{
  "triggers": [
      {
        "id": "receive_data_from_tcp_server",
        "ref": "#tcp_client",
        "name": "Receive Data from TCP server",
        "description": "Simple TCP Client Trigger",
        "settings": {
          "host": "127.0.0.1",
          "port": "64782",
          "delimiter": "\n",
          "timeout": "10000"
        },
        "handlers": [
          {
            "action": {
              "ref": "#flow",
              "settings": {
                "flowURI": "res://flow:read_remote_tcp"
              },
              "input": {
                "input_ais": "=$.data"
              }
            }
          }
        ]
      }
    ]
}
```