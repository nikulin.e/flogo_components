package tcp_client

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"io"
	"net"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/project-flogo/core/data/metadata"
	"github.com/project-flogo/core/support/log"
	"github.com/project-flogo/core/trigger"
)

var triggerMd = trigger.NewMetadata(&Settings{}, &HandlerSettings{}, &Output{}, &Reply{})

func init() {
	_ = trigger.Register(&Trigger{}, &Factory{})
}

// Factory is a kafka trigger factory
type Factory struct {
}

// Metadata implements trigger.Factory.Metadata
func (*Factory) Metadata() *trigger.Metadata {
	return triggerMd
}

// New implements trigger.Factory.New
func (*Factory) New(config *trigger.Config) (trigger.Trigger, error) {
	s := &Settings{}
	err := metadata.MapToStruct(config.Settings, s, true)
	if err != nil {
		return nil, err
	}

	return &Trigger{settings: s}, nil
}

// Trigger struct
type Trigger struct {
	settings   *Settings
	handlers   []trigger.Handler
	connection net.Conn
	logger     log.Logger
	delimiter  byte
}

// Initialize initializes the trigger
func (t *Trigger) Initialize(ctx trigger.InitContext) error {
	port := t.settings.Port
	t.handlers = ctx.GetHandlers()
	t.logger = ctx.Logger()

	delimiter := t.settings.Delimiter

	if delimiter != "" {
		r, _ := utf8.DecodeRuneInString(delimiter)
		t.delimiter = byte(r)
	}

	if port == "" {
		return errors.New("Valid port must be set")
	}
	return nil
}

// Start trigger
func (t *Trigger) Start() error {
	go t.startRead()
	t.logger.Infof("Started read from %s: %s", t.settings.Host, t.settings.Port)
	return nil
}

func (t *Trigger) startRead() {
	for {
		// Try to connect
		conn, err := net.Dial("tcp", t.settings.Host+":"+t.settings.Port)
		if err != nil {
			t.logger.Errorf("Error on connect to %s: %s. %s", t.settings.Host, t.settings.Port, err)
			continue
		}
		t.connection = conn
		t.handleNewConnection(conn)
	}
}

func (t *Trigger) handleNewConnection(conn net.Conn) {

	for {
		if t.settings.TimeOut > 0 {
			t.logger.Info("Setting timeout: ", t.settings.TimeOut)
			conn.SetDeadline(time.Now().Add(time.Duration(t.settings.TimeOut) * time.Millisecond))
		}

		output := &Output{}

		if t.delimiter != 0 {
			data, err := bufio.NewReader(conn).ReadBytes(t.delimiter)
			if err != nil {
				errString := err.Error()
				if !strings.Contains(errString, "use of closed network connection") {
					t.logger.Error("Error reading data from connection: ", err.Error())
				} else {
					t.logger.Info("Connection is closed.")
				}
				if nerr, ok := err.(net.Error); !ok || !nerr.Timeout() {
					// Return if not timeout error
					return
				}

			} else {
				output.Data = string(data[:len(data)-1])
			}
		} else {
			var buf bytes.Buffer
			_, err := io.Copy(&buf, conn)
			if err != nil {
				errString := err.Error()
				if !strings.Contains(errString, "use of closed network connection") {
					t.logger.Error("Error reading data from connection: ", err.Error())
				} else {
					t.logger.Info("Connection is closed.")
				}
				if nerr, ok := err.(net.Error); !ok || !nerr.Timeout() {
					// Return if not timeout error
					return
				}
			} else {
				output.Data = string(buf.Bytes())
			}
		}

		if output.Data != "" {
			var replyData []string
			for i := 0; i < len(t.handlers); i++ {
				results, err := t.handlers[i].Handle(context.Background(), output)
				if err != nil {
					t.logger.Error("Error invoking action : ", err.Error())
					continue
				}

				reply := &Reply{}
				err = reply.FromMap(results)
				if err != nil {
					t.logger.Error("Failed to convert flow output : ", err.Error())
					continue
				}
				if reply.Reply != "" {
					replyData = append(replyData, reply.Reply)
				}
			}

			if len(replyData) > 0 {
				replyToSend := strings.Join(replyData, string(t.delimiter))
				// Send a response back to client contacting us.
				_, err := conn.Write([]byte(replyToSend + "\n"))
				if err != nil {
					t.logger.Error("Failed to write to connection : ", err.Error())
				}
			}
		}
	}
}

// Stop implements ext.Trigger.Stop
func (t *Trigger) Stop() error {

	t.connection.Close()
	t.connection = nil
	t.logger.Info("Stopped connection")

	return nil
}
